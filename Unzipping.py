import os
import tarfile
from urllib.request import URLopener
import shutil
import gzip
from glob import glob

months = {'01_Jan': 31, '02_Feb': 29, '03_Mar': 31, '04_Apr': 30, '05_May': 31, '06_Jun': 30,
          '07_Jul': 31, '08_Aug': 31, '09_Sep': 30, '10_Oct': 31, '11_Nov': 30, '12_Dec': 31
          }
months_list = sorted(months.keys())

def download_mohsen(url, year, month, day, zipped_file):

    zipped_url = url + '/{}/{}/{}'.format(year, month, zipped_file)
    opener = URLopener()

    with opener.open(zipped_url) as remote_file, open(zipped_file,'wb') as local_file:
        shutil.copyfileobj(remote_file, local_file)
            

def decompress_mohsen(zipped_file, zipped_output):

    tar = tarfile.open(zipped_file)
    tar.extractall(zipped_output)
    tar.close()
    os.remove(zipped_file)


def decompress_gz(gzip_path):
    """Decompress the zip files and delete the compressed one."""


    gzip_file = gzip.open(gzip_path) # use gzip.open instead of builtin open function
    file_content = gzip_file.read()
    with open(gzip_path[:-3],'wb') as f:
        f.write(file_content)
    gzip_file.close()
    os.remove(gzip_path)
    



    
if __name__ == "__main__":
    
    url = 'ftp://sidads.colorado.edu/DATASETS/NOAA/G02158/masked'
    for year in range(2004, 2018):
        for day in range(1,32):
            for m in months_list:
                
                zipped_file = 'SNODAS_{}{}{:02d}.tar'.format(year, m[:2], day)
                zipped_output = './{}/{}/{}'.format(year, m, day)
                try:
                    download_mohsen(url, year, m, day, zipped_file)
                    decompress_mohsen(zipped_file, zipped_output)
                    gz_files=glob(zipped_output+'/*.gz')
                    for gz_file in gz_files:
                        decompress_gz(gz_file)
                except:
                    if day <= months[m]:
                        print('file for {} {} {} does not exist'.format(year, m, day))

                        
    


    

